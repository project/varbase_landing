# Varbase Landing Page (Paragraphs)

This is a [Varbase](https://www.drupal.org/project/varbase) Feature.

Provides the basis for Landing Pages, which are built to include appealing
 stacked components that are visually separate.

## [Varbase documentation](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/optional-components/varbase-landing-page-paragraphs)
Check out Varbase documentation for more details.

* [Varbase Landing Page (Paragraphs) Module](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/optional-components/varbase-landing-page-paragraphs)


Join Our Slack Team for Feedback and Support
http://slack.varbase.vardot.com/

This module is sponsored and developed by [Vardot](https://www.drupal.org/vardot).
